import java.util.List;

public class Grille {

    private int nbLigne;
    private int nbColonnes;
    private int nbBombe;

    //Constructeur de la class Grille contenant trois argument
    public Grille(int nbLigne, int nbColonnes, int nbBombe) {
        this.nbLigne = nbLigne;
        this.nbColonnes = nbColonnes;
        this.nbBombe = nbBombe;
    }


    //ensemble des getters des arguments
    public int getNbLigne() {
        return nbLigne;
    }

    public int getNbColonnes() {
        return nbColonnes;
    }

    public int getNbBombe() {
        return nbBombe;
    }

    //retourne la case à une certaine coordonné 
    public Case getCase(int i, int j){ 
        List<Case> caseVoisine= Case.getCasesVoisines(); 
        Case case1=new Case(caseVoisine);
        return case1;
    }
    
    //retourne le nombre de case revelees sur le plateau
    public int getNombreDeCasesRevelees(){
        return 1;
    }

    //retourne le nombre de case marqué donc ayant une bombe dessous d'après le joueur
    public int getNombreDeCasesMarquees(){
        return 1;
    }

    //pour savoir si la partie est perdu
    public boolean estPerdu(){
        return false;
    }

    //pour savoir si la partie est gagné (toute les case vide son révélé)
    public boolean estGagne(){
        return false;
    }


}
