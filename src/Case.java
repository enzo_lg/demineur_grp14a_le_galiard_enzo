import java.util.List;
import java.util.ArrayList;

public class Case {
    
    private boolean bombe;
    private boolean revelee;
    private boolean marquee;
    private List<Case> casesVoisines;

    public Case(List<Case> casesVoisines){
        this.bombe=false;
        this.revelee=false;
        this.marquee=false;
        this.casesVoisines= new ArrayList<>();

    }

    public void ajouteBombe(){
        this.bombe = true;
    }

    public Boolean estBombe(){
        return this.bombe;
    }

    public Boolean estRevelee(){
        return this.revelee;
    }

    public Boolean estMarquee(){
        return this.marquee;
    }

    public void reveler(boolean revelee){
        this.revelee=revelee;
    }

    public void marquer(boolean marquee){
        this.marquee=marquee;
    }

    public static List<Case> getCasesVoisines(){
        List<Case> casesVoisines = new ArrayList<>();
        return casesVoisines;
    }

    public void ajouteCaseVoisine(Case c){
        List<Case> casesVoisines = new ArrayList<>();
        casesVoisines.add(c);
    }

    public int getNbBombesVoisines(){
        List<Case> casesVoisines = new ArrayList<>();
        int cpt = 0;
        for(Case c:casesVoisines){
            if(c.estBombe()){
                cpt+=1;
            }
        }
        return cpt;
    }

    public String getAffichage(){
        List<Case> casesVoisines = new ArrayList<>();
        if(!estRevelee() && !estMarquee()){
            return " ";
        }
        if(estBombe() && estRevelee()){
            return "@";
        }
        if(estMarquee()){
            return "?";
        }
        else{
            if(estRevelee()){
            int cpt = 0;
            boolean res = true;
            for(Case c: casesVoisines){
                if (c.estBombe()){
                    cpt+=1;
                    res=false;
                }
            if(res){
                return "0";
            }
            if(cpt>0){
                return ""+cpt;
            }
            }
            }
        }
        return null;
    }
}

